﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql;
using MySql.Data;
using System.Drawing.Imaging;
using Dicom.Imaging;
using Dicom.IO.Buffer;
using Dicom;
using System.Runtime.InteropServices;
using System.Globalization;
using System.IO;
using Dicom.Network;

namespace TPACSEKGImport
{
    public partial class frmMain : Form
    {
        private CultureInfo _defaultculinf = null;

        string _pacsaddress = System.Configuration.ConfigurationManager.AppSettings.Get("PACSServerAddress");
        string _pacsport = System.Configuration.ConfigurationManager.AppSettings.Get("PACSServerPort");
        string _pacscallingae = System.Configuration.ConfigurationManager.AppSettings.Get("PACSCallingAE");
        string _pacscalledae = System.Configuration.ConfigurationManager.AppSettings.Get("PACSCalledAE");
        //get current path of
        string _currpath = System.IO.Path.GetDirectoryName(Application.ExecutablePath);
        string _tmppathname = System.Configuration.ConfigurationManager.AppSettings.Get("TempPath");

        public frmMain()
        {
            InitializeComponent();
            _defaultculinf = new CultureInfo("en-US");

            setlstselectedfilecolumnwidth(lstfilelist);
        }

        private void cmd_hissearch_Click(object sender, EventArgs e)
        {
            //send search parameter to frmsearch result

            //open frmsearchresult

        }


        #region  lstselectfile
        private void setlstselectedfilecolumnwidth(ListView lv)
        {
            int x = lv.Width / 1 == 0 ? 1 : lv.Width / 1;
            lv.Columns[0].Width = x - 5;
        }
        private void lstfilelist_DoubleClick(object sender, EventArgs e)
        {

            //Display Image from selected listview
            img_preview.Image = Bitmap.FromFile(lstfilelist.SelectedItems[0].SubItems[0].Text);

        }

        private void cmd_deletefile_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem lstitem in lstfilelist.SelectedItems)
            {
                lstitem.Remove();
            }
        }

        private void cmd_addfile_Click(object sender, EventArgs e)
        {
            // open select file dialogue
            OpenFileDialog ofdselectfile = new OpenFileDialog();
            ofdselectfile.Filter = "jpg (*.jpg)|*.jpg|jpeg (*.jpeg)|*.jpeg";
            ofdselectfile.FilterIndex = 1;
            ofdselectfile.RestoreDirectory = true;

            if (ofdselectfile.ShowDialog() == DialogResult.OK)
            {
                lstfilelist.Items.Add(ofdselectfile.FileName);
            }

        }

        #endregion

        private void cmd_sendtopacs_Click(object sender, EventArgs e)
        {
            
            //combind with temppath name configuration
            string _tmppath = Path.Combine(_currpath, _tmppathname);
            //check existing temp path
            if (!Directory.Exists(_tmppath))
            {
                Directory.CreateDirectory(_tmppath);
            }
            //retrive file from list
            int _totalno = lstfilelist.Items.Count;
            DicomUID studyUID = GenerateUid();
           
            UTF8Encoding utf8 = new UTF8Encoding();
            string patientname = txt_hisfname.Text;
            patientname = coverttoUTF8(patientname);

            for (int i = 0; i < _totalno; i++)
            {
                string lstfile = lstfilelist.Items[i].Text;
                //create dicom file
                string _savefilename = Path.Combine(_tmppath, "dicomfile_" + (i + 1).ToString() + ".dcm");
                ImportImage(lstfile, studyUID, (i + 1).ToString(), (i + 1).ToString(), _totalno.ToString(), _savefilename,patientname);

                senddicomtopacs(_savefilename);

                File.Delete(_savefilename);

            }
            
        }
        private string coverttoUTF8(string input)
        {
            System.Text.Encoding utf_8 = System.Text.Encoding.UTF8;

            //string to utf
            byte[] utf = System.Text.Encoding.UTF8.GetBytes(input);

            //utf to string
            string s2 = System.Text.Encoding.UTF8.GetString(utf);
            return s2;
        }
        #region SendDicomtoPACS
        private void senddicomtopacs(string dicomfile)
        {
            var client = new DicomClient();
            client.AddRequest(new DicomCStoreRequest(dicomfile));
            client.Send(_pacsaddress, Convert.ToInt32(_pacsport), false, _pacscallingae, _pacscalledae);
        }
    
        #endregion

        #region ConvertImg2Dicom
        private void ImportImage(string file,DicomUID studyUID,string studyid,string seriesno,string totalno,string savedicomfilename,string patientname)
        {
            Bitmap bitmap = new Bitmap(file);
            bitmap = GetValidImage(bitmap);
            int rows, columns;
            byte[] pixels = GetPixels(bitmap, out rows, out columns);
            MemoryByteBuffer buffer = new MemoryByteBuffer(pixels);
            DicomDataset dataset = new DicomDataset();
            dataset.AddOrUpdate(DicomTag.SpecificCharacterSet, "ISO_IR 192");
            FillDataset(dataset,studyUID,studyid,seriesno,totalno,patientname);
            dataset.Add(DicomTag.PhotometricInterpretation, PhotometricInterpretation.Rgb.Value);
            dataset.Add(DicomTag.Rows, (ushort)rows);
            dataset.Add(DicomTag.Columns, (ushort)columns);
            dataset.Add(DicomTag.BitsAllocated, (ushort)8);
            DicomPixelData pixelData = DicomPixelData.Create(dataset, true);
            pixelData.BitsStored = 8;
            pixelData.BitsAllocated = 8;
            pixelData.SamplesPerPixel = 3;
            pixelData.HighBit = 7;
            pixelData.PixelRepresentation = 0;
            pixelData.PlanarConfiguration = 0;
            pixelData.AddFrame(buffer);

            DicomFile dicomfile = new DicomFile(dataset);
            dicomfile.Save(savedicomfilename);
        }

        private void FillDataset(DicomDataset dataset,DicomUID studyUID,string studyid,string seriesno,string totalno,string patientname)
        {

            //type 1 attributes.

            dataset.Add(DicomTag.SOPClassUID, DicomUID.ComputedRadiographyImageStorage);
            dataset.Add(DicomTag.StudyInstanceUID, studyUID);
            dataset.Add(DicomTag.SeriesInstanceUID, GenerateUid());
            dataset.Add(DicomTag.SOPInstanceUID, GenerateUid());
            
            //type 2 attributes
            dataset.Add(DicomTag.PatientID, txt_hishn.Text.Trim());
            dataset.Add(DicomTag.PatientName, patientname);
            dataset.Add(DicomTag.PatientBirthDate, dtp_hisbirthdate.Value.ToString("yyyyMMdd",_defaultculinf));
            dataset.Add(DicomTag.PatientSex, cmb_hissex.Text );
            dataset.Add(DicomTag.StudyDate, dtp_hisstudydate.Value.ToString("yyyyMMdd",_defaultculinf));
            dataset.Add(DicomTag.StudyTime, dtp_hisstudytime.Value.ToString("HHmmss",_defaultculinf));
            dataset.Add(DicomTag.AccessionNumber, string.Empty);
            dataset.Add(DicomTag.ReferringPhysicianName, string.Empty);
            dataset.Add(DicomTag.StudyID, studyid);
            dataset.Add(DicomTag.SeriesNumber, seriesno);
            dataset.Add(DicomTag.ModalitiesInStudy, "CR");
            dataset.Add(DicomTag.Modality, "CR");
            dataset.Add(DicomTag.NumberOfStudyRelatedInstances, totalno);
            dataset.Add(DicomTag.NumberOfStudyRelatedSeries, totalno);
            dataset.Add(DicomTag.NumberOfSeriesRelatedInstances, totalno);
            dataset.Add(DicomTag.PatientOrientation, "F/A");
            dataset.Add(DicomTag.ImageLaterality, "U");
        }
        private DicomUID GenerateUid()
        {
            StringBuilder uid = new StringBuilder();
            //uid.Append("1.08.1982.10121984.2.0.07").Append('.').Append(DateTime.UtcNow.Ticks);
            uid.Append("1.2.276.0.7230010.3.1").Append('.').Append(DateTime.UtcNow.Ticks);
            return new DicomUID(uid.ToString(), "SOP Instance UID", DicomUidType.SOPInstance);
        }
     

        private Bitmap GetValidImage(Bitmap bitmap)
        {
            if (bitmap.PixelFormat != PixelFormat.Format24bppRgb)
            {
                Bitmap old = bitmap;
                using (old)
                {
                    bitmap = new Bitmap(old.Width, old.Height, PixelFormat.Format24bppRgb);
                    using (System.Drawing.Graphics g = System.Drawing.Graphics.FromImage(bitmap))
                    {
                        g.DrawImage(old, 0, 0, old.Width, old.Height);
                    }
                }
            }
            return bitmap;
        }

        private  byte[] GetPixels(Bitmap image, out int rows, out int columns)
        {
            rows = image.Height;
            columns = image.Width;

            if (rows % 2 != 0 && columns % 2 != 0)
                --columns;

            BitmapData data = image.LockBits(new Rectangle(0, 0, columns, rows), ImageLockMode.ReadOnly, image.PixelFormat);
            IntPtr bmpData = data.Scan0;
            try
            {
                int stride = columns * 3;
                int size = rows * stride;
                byte[] pixelData = new byte[size];
                for (int i = 0; i < rows; ++i)
                    Marshal.Copy(new IntPtr(bmpData.ToInt64() + i * data.Stride), pixelData, i * stride, stride);

                //swap BGR to RGB
                SwapRedBlue(pixelData);
                return pixelData;
            }
            finally
            {
                image.UnlockBits(data);
            }
        }
        private  void SwapRedBlue(byte[] pixels)
        {
            for (int i = 0; i < pixels.Length; i += 3)
            {
                byte temp = pixels[i];
                pixels[i] = pixels[i + 2];
                pixels[i + 2] = temp;
            }
        }
        #endregion


    }
}
