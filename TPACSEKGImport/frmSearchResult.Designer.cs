﻿namespace TPACSEKGImport
{
    partial class frmSearchResult
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmd_resultok = new System.Windows.Forms.Button();
            this.cmd_resultcancle = new System.Windows.Forms.Button();
            this.lst_searchresult = new System.Windows.Forms.ListView();
            this.c_resulthn = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.c_resultfname = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.c_resultdob = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.c_resultsex = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.SuspendLayout();
            // 
            // cmd_resultok
            // 
            this.cmd_resultok.Location = new System.Drawing.Point(414, 321);
            this.cmd_resultok.Name = "cmd_resultok";
            this.cmd_resultok.Size = new System.Drawing.Size(75, 23);
            this.cmd_resultok.TabIndex = 1;
            this.cmd_resultok.Text = "OK";
            this.cmd_resultok.UseVisualStyleBackColor = true;
            // 
            // cmd_resultcancle
            // 
            this.cmd_resultcancle.Location = new System.Drawing.Point(510, 321);
            this.cmd_resultcancle.Name = "cmd_resultcancle";
            this.cmd_resultcancle.Size = new System.Drawing.Size(75, 23);
            this.cmd_resultcancle.TabIndex = 2;
            this.cmd_resultcancle.Text = "Cancle";
            this.cmd_resultcancle.UseVisualStyleBackColor = true;
            // 
            // lst_searchresult
            // 
            this.lst_searchresult.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.c_resulthn,
            this.c_resultfname,
            this.c_resultdob,
            this.c_resultsex});
            this.lst_searchresult.Location = new System.Drawing.Point(12, 12);
            this.lst_searchresult.Name = "lst_searchresult";
            this.lst_searchresult.Size = new System.Drawing.Size(573, 303);
            this.lst_searchresult.TabIndex = 3;
            this.lst_searchresult.UseCompatibleStateImageBehavior = false;
            this.lst_searchresult.View = System.Windows.Forms.View.Details;
            // 
            // c_resulthn
            // 
            this.c_resulthn.Text = "HN";
            // 
            // c_resultfname
            // 
            this.c_resultfname.Text = "ชื่อ-นามสกุล";
            // 
            // c_resultdob
            // 
            this.c_resultdob.Text = "วัน-เดือน-ปีเกิด";
            // 
            // c_resultsex
            // 
            this.c_resultsex.Text = "เพศ";
            // 
            // frmSearchResult
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(597, 354);
            this.Controls.Add(this.lst_searchresult);
            this.Controls.Add(this.cmd_resultcancle);
            this.Controls.Add(this.cmd_resultok);
            this.Name = "frmSearchResult";
            this.Text = "frmSearchResult";
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button cmd_resultok;
        private System.Windows.Forms.Button cmd_resultcancle;
        private System.Windows.Forms.ListView lst_searchresult;
        private System.Windows.Forms.ColumnHeader c_resulthn;
        private System.Windows.Forms.ColumnHeader c_resultfname;
        private System.Windows.Forms.ColumnHeader c_resultdob;
        private System.Windows.Forms.ColumnHeader c_resultsex;
    }
}