﻿namespace TPACSEKGImport
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.ColumnHeader c_filename;
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cmd_hissearch = new System.Windows.Forms.Button();
            this.dtp_hisstudytime = new System.Windows.Forms.DateTimePicker();
            this.dtp_hisstudydate = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.cmb_hissex = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.dtp_hisbirthdate = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.txt_hisfname = new System.Windows.Forms.TextBox();
            this.lblfname = new System.Windows.Forms.Label();
            this.txt_hishn = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.cmd_deletefile = new System.Windows.Forms.Button();
            this.lstfilelist = new System.Windows.Forms.ListView();
            this.cmd_addfile = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.img_preview = new System.Windows.Forms.PictureBox();
            this.cmd_sendtopacs = new System.Windows.Forms.Button();
            c_filename = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.img_preview)).BeginInit();
            this.SuspendLayout();
            // 
            // c_filename
            // 
            c_filename.Text = "Filename";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cmd_hissearch);
            this.groupBox1.Controls.Add(this.dtp_hisstudytime);
            this.groupBox1.Controls.Add(this.dtp_hisstudydate);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.cmb_hissex);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.dtp_hisbirthdate);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txt_hisfname);
            this.groupBox1.Controls.Add(this.lblfname);
            this.groupBox1.Controls.Add(this.txt_hishn);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(937, 88);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Patient Information";
            // 
            // cmd_hissearch
            // 
            this.cmd_hissearch.Enabled = false;
            this.cmd_hissearch.Location = new System.Drawing.Point(853, 59);
            this.cmd_hissearch.Name = "cmd_hissearch";
            this.cmd_hissearch.Size = new System.Drawing.Size(75, 23);
            this.cmd_hissearch.TabIndex = 11;
            this.cmd_hissearch.Text = "ค้นหา";
            this.cmd_hissearch.UseVisualStyleBackColor = true;
            this.cmd_hissearch.Click += new System.EventHandler(this.cmd_hissearch_Click);
            // 
            // dtp_hisstudytime
            // 
            this.dtp_hisstudytime.CustomFormat = "HH:mm:ss";
            this.dtp_hisstudytime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtp_hisstudytime.Location = new System.Drawing.Point(241, 48);
            this.dtp_hisstudytime.Name = "dtp_hisstudytime";
            this.dtp_hisstudytime.ShowUpDown = true;
            this.dtp_hisstudytime.Size = new System.Drawing.Size(69, 20);
            this.dtp_hisstudytime.TabIndex = 10;
            // 
            // dtp_hisstudydate
            // 
            this.dtp_hisstudydate.CustomFormat = "dd - MMMM - yyyy";
            this.dtp_hisstudydate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtp_hisstudydate.Location = new System.Drawing.Point(98, 48);
            this.dtp_hisstudydate.Name = "dtp_hisstudydate";
            this.dtp_hisstudydate.Size = new System.Drawing.Size(137, 20);
            this.dtp_hisstudydate.TabIndex = 9;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 54);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(86, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Study Date Time";
            // 
            // cmb_hissex
            // 
            this.cmb_hissex.FormattingEnabled = true;
            this.cmb_hissex.Items.AddRange(new object[] {
            "F",
            "M"});
            this.cmb_hissex.Location = new System.Drawing.Point(855, 20);
            this.cmb_hissex.Name = "cmb_hissex";
            this.cmb_hissex.Size = new System.Drawing.Size(69, 21);
            this.cmb_hissex.TabIndex = 7;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(823, 24);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(28, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "เพศ";
            // 
            // dtp_hisbirthdate
            // 
            this.dtp_hisbirthdate.CustomFormat = "dd - MMMM - yyyy";
            this.dtp_hisbirthdate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtp_hisbirthdate.Location = new System.Drawing.Point(664, 21);
            this.dtp_hisbirthdate.Name = "dtp_hisbirthdate";
            this.dtp_hisbirthdate.Size = new System.Drawing.Size(153, 20);
            this.dtp_hisbirthdate.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(579, 24);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(79, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "วัน-เดือน-ปีเกิด";
            // 
            // txt_hisfname
            // 
            this.txt_hisfname.Location = new System.Drawing.Point(241, 21);
            this.txt_hisfname.Name = "txt_hisfname";
            this.txt_hisfname.Size = new System.Drawing.Size(332, 20);
            this.txt_hisfname.TabIndex = 3;
            // 
            // lblfname
            // 
            this.lblfname.AutoSize = true;
            this.lblfname.Location = new System.Drawing.Point(173, 24);
            this.lblfname.Name = "lblfname";
            this.lblfname.Size = new System.Drawing.Size(62, 13);
            this.lblfname.TabIndex = 2;
            this.lblfname.Text = "ชื่อ-นามสกุล";
            // 
            // txt_hishn
            // 
            this.txt_hishn.Location = new System.Drawing.Point(35, 21);
            this.txt_hishn.Name = "txt_hishn";
            this.txt_hishn.Size = new System.Drawing.Size(132, 20);
            this.txt_hishn.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(23, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "HN";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.cmd_deletefile);
            this.groupBox2.Controls.Add(this.lstfilelist);
            this.groupBox2.Controls.Add(this.cmd_addfile);
            this.groupBox2.Location = new System.Drawing.Point(12, 106);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(937, 129);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "EKG Files";
            // 
            // cmd_deletefile
            // 
            this.cmd_deletefile.Location = new System.Drawing.Point(853, 48);
            this.cmd_deletefile.Name = "cmd_deletefile";
            this.cmd_deletefile.Size = new System.Drawing.Size(75, 23);
            this.cmd_deletefile.TabIndex = 2;
            this.cmd_deletefile.Text = "Delete";
            this.cmd_deletefile.UseVisualStyleBackColor = true;
            this.cmd_deletefile.Click += new System.EventHandler(this.cmd_deletefile_Click);
            // 
            // lstfilelist
            // 
            this.lstfilelist.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            c_filename});
            this.lstfilelist.GridLines = true;
            this.lstfilelist.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.lstfilelist.HideSelection = false;
            this.lstfilelist.Location = new System.Drawing.Point(9, 19);
            this.lstfilelist.Name = "lstfilelist";
            this.lstfilelist.Size = new System.Drawing.Size(838, 97);
            this.lstfilelist.TabIndex = 1;
            this.lstfilelist.UseCompatibleStateImageBehavior = false;
            this.lstfilelist.View = System.Windows.Forms.View.Details;
            this.lstfilelist.DoubleClick += new System.EventHandler(this.lstfilelist_DoubleClick);
            // 
            // cmd_addfile
            // 
            this.cmd_addfile.Location = new System.Drawing.Point(853, 19);
            this.cmd_addfile.Name = "cmd_addfile";
            this.cmd_addfile.Size = new System.Drawing.Size(75, 23);
            this.cmd_addfile.TabIndex = 0;
            this.cmd_addfile.Text = "Add";
            this.cmd_addfile.UseVisualStyleBackColor = true;
            this.cmd_addfile.Click += new System.EventHandler(this.cmd_addfile_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.img_preview);
            this.groupBox3.Location = new System.Drawing.Point(12, 241);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(937, 389);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Preview";
            // 
            // img_preview
            // 
            this.img_preview.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.img_preview.Location = new System.Drawing.Point(9, 19);
            this.img_preview.Name = "img_preview";
            this.img_preview.Size = new System.Drawing.Size(919, 364);
            this.img_preview.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.img_preview.TabIndex = 0;
            this.img_preview.TabStop = false;
            // 
            // cmd_sendtopacs
            // 
            this.cmd_sendtopacs.Location = new System.Drawing.Point(874, 636);
            this.cmd_sendtopacs.Name = "cmd_sendtopacs";
            this.cmd_sendtopacs.Size = new System.Drawing.Size(75, 23);
            this.cmd_sendtopacs.TabIndex = 3;
            this.cmd_sendtopacs.Text = "Send";
            this.cmd_sendtopacs.UseVisualStyleBackColor = true;
            this.cmd_sendtopacs.Click += new System.EventHandler(this.cmd_sendtopacs_Click);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(961, 671);
            this.Controls.Add(this.cmd_sendtopacs);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "frmMain";
            this.Text = "TPACS EKG Import";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.img_preview)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblfname;
        private System.Windows.Forms.TextBox txt_hishn;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txt_hisfname;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker dtp_hisbirthdate;
        private System.Windows.Forms.ComboBox cmb_hissex;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DateTimePicker dtp_hisstudydate;
        private System.Windows.Forms.DateTimePicker dtp_hisstudytime;
        private System.Windows.Forms.Button cmd_hissearch;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button cmd_addfile;
        private System.Windows.Forms.ListView lstfilelist;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button cmd_sendtopacs;
        private System.Windows.Forms.Button cmd_deletefile;
        private System.Windows.Forms.PictureBox img_preview;
    }
}

